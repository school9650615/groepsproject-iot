import time
from bmp280 import BMP280
from smbus2 import SMBus, i2c_msg
import wiringpi as wp
import sys
import paho.mqtt.client as mqtt
import os
import threading

# Create an I2C bus object
bus = SMBus(0)
addressLightLevel = 0x23 # i2c address
addressPressureAndTemperature = 0x76 

# Setup BH1750
bus.write_byte(addressLightLevel, 0x10)
bytes_read = bytearray(2)

# Setup BMP280
bmp280 = BMP280(i2c_addr= addressPressureAndTemperature, i2c_dev=bus)
mqttUpdateInterval = 15 # Sample period in seconds

MQTT_HOST ="mqtt3.thingspeak.com"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
BMP_CHANNEL_ID = "2492588" # temperature and air pressure
# BMP_CHANNEL_ID = os.getenv('GROUPPROJECT_BMP_CHANNEL_ID') # temperature and air pressure
MQTT_BMP_TOPIC = f"channels/{BMP_CHANNEL_ID}/publish"
MQTT_BMP_TOPIC_SUBSCRIBE = f"channels/{BMP_CHANNEL_ID}/subscribe/fields"
BH_CHANNEL_ID = "2502066" # light intensity
# BH_CHANNEL_ID = os.getenv('GROUPPROJECT_BH_CHANNEL_ID') # light intensity
MQTT_BH_TOPIC = f"channels/{BH_CHANNEL_ID}/publish"
MQTT_CLIENT_ID = "BTsKIhsyICkuLzYVBRMEJQA"
MQTT_USER = "BTsKIhsyICkuLzYVBRMEJQA"
MQTT_PWD = "hbIB2HGxYBygBCiuJp9R9Lh1"
# MQTT_CLIENT_ID = os.getenv('GROUPPROJECT_MQTT_CLIENT_ID')
# MQTT_USER = os.getenv('GROUPPROJECT_MQTT_USER')
# MQTT_PWD = os.getenv('GROUPPROJECT_MQTT_PWD')

def on_connect(client, userdata, flags, rc):
    if rc==0:
        print("Connected OK with result code "+str(rc))
    else:
        print("Bad connection with result code "+str(rc))

def on_disconnect(client, userdata, flags, rc=0):
    print("Disconnected result code "+ str(rc))

def on_message(client,userdata,msg):
    print("Received a message on topic: " + msg.topic + "; message: " + msg.payload)

def get_lux_value(bus, address):
    write = i2c_msg.write(address, [0x10]) # 1lx resolution 120ms see datasheet
    read = i2c_msg.read(address, 2)
    bus.i2c_rdwr(write, read)
    bytes_read = list(read)
    return (((bytes_read[0]&3)<<8) + bytes_read[1])/1.2 # conversion see datasheet

# Set up a MQTT Client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, MQTT_CLIENT_ID)
client.username_pw_set(MQTT_USER, MQTT_PWD)

# Connect callback handlers to client
client.on_connect= on_connect
client.on_disconnect= on_disconnect
client.on_message= on_message

print("Attempting to connect to %s" % MQTT_HOST)
client.connect(MQTT_HOST, MQTT_PORT)
client.loop_start() #start the loop

# region Software PWM RGB LED
import time
import wiringpi as wp
import threading

# Global variables
minBrightness = 0
maxBrightness = 100
pause_time = 0.00204
currentBrightness = -1
redPin = 13
greenPin = 10
bluePin = 9
rgbPins = [redPin, greenPin, bluePin]
pin_CS_adc = 16

# Define a threading Lock
control_lock = threading.Lock()

# RGB LED Functions
def controlLEDs(cnt):
    with control_lock:
        for pin in rgbPins:
            wp.softPwmWrite(pin, cnt)

def softPwmCreate(_pin):
    wp.softPwmCreate(_pin, minBrightness, maxBrightness)

# ADC Functions
def activateADC ():
    wp.digitalWrite(pin_CS_adc, 0)
    time.sleep(0.000005)

def deactivateADC():
    wp.digitalWrite(pin_CS_adc, 1)
    time.sleep(0.000005)

def readADC(adcnum):
    if not (0 <= adcnum <= 7):
        return -1
    activateADC()
    revlen, recv_data = wp.wiringPiSPIDataRW(1, bytes([1, (8 + adcnum) << 4, 0]))
    time.sleep(0.000005)
    deactivateADC()
    return ((recv_data[1] & 3) << 8) + recv_data[2]

def determineBrightness():
    potentiometer_value = readADC(0)
    brightness = round((potentiometer_value / 1023) * maxBrightness)
    return brightness

# SETUP RGB LED WITH SOFTWARE PWM
wp.wiringPiSetup()
wp.pinMode(pin_CS_adc, 1)
wp.wiringPiSPISetupMode(1, 0, 500000, 0)

# Set RGB pins as a softPWM output
for pin in rgbPins:
    softPwmCreate(pin)
# endregion

def on_chart7_message(client, userdata, msg):
    print()
    print("Received a message for Wendy's temperature reading on topic: " + msg.topic + "; message: " + msg.payload.decode())

def on_chart8_message(client, userdata, msg):
    print()
    print("Received a message for Wendy's pressure reading on topic: " + msg.topic + "; message: " + msg.payload.decode())

# Set the callback for messages on chart 7 topic
client.message_callback_add(MQTT_BMP_TOPIC_SUBSCRIBE + "/field7", on_chart7_message)
client.message_callback_add(MQTT_BMP_TOPIC_SUBSCRIBE + "/field8", on_chart8_message)

# MAIN
try:
    lastMQTTUpdate = 0
    currentMQTTUpdate = 0
    pwmTimer = 0.0005

    client.subscribe(topic=MQTT_BMP_TOPIC_SUBSCRIBE + "/field7") # Subscribes to Wendy's temperature readings
    client.subscribe(topic=MQTT_BMP_TOPIC_SUBSCRIBE + "/field8") # Subscribes to Wendy's air pressure readings

    while True:
        currentMQTTUpdate = currentMQTTUpdate + pwmTimer
        # POTENTIOMETER UPDATE
        brightness = determineBrightness()

        if brightness != currentBrightness:
            currentBrightness = brightness
            controlLEDs(currentBrightness)
            print(f"Required Brightness [{minBrightness} - {maxBrightness}]: {brightness} %")

        # MQTT UPDATE
        if ((currentMQTTUpdate - lastMQTTUpdate) >= mqttUpdateInterval):
            lastMQTTUpdate = currentMQTTUpdate
            bh1750_lux = get_lux_value(bus, addressLightLevel)
            bmp280_temperature = bmp280.get_temperature()
            bmp280_pressure = bmp280.get_pressure()

            print()
            print("Light Level: {:.2f} Lux".format(bh1750_lux) + ", Temperature: %4.1f °C, Pressure: %4.1f hPa" % (bmp280_temperature, bmp280_pressure))

            # Create the JSON data structure
            MQTT_BMP_DATA = "field5=" + str(bmp280_temperature) + "&field6="+str(bmp280_pressure) + "&status=MQTTPUBLISH"
            MQTT_BH_DATA ="field2=" + str( bh1750_lux) + "&status=MQTTPUBLISH"
            # print(MQTT_BMP_DATA)
            # print(MQTT_BH_DATA)

            client.publish(topic=MQTT_BMP_TOPIC, payload=MQTT_BMP_DATA, qos=0, retain=False, properties=None)
            client.publish(topic=MQTT_BH_TOPIC, payload=MQTT_BH_DATA, qos=0, retain=False, properties=None)

        time.sleep(pwmTimer)
except OSError:
    client.reconnect()
except KeyboardInterrupt:
    client.disconnect()  # Disconnect from the broker
    for pin in rgbPins:
        wp.softPwmWrite(pin, 0)
    deactivateADC()
    print("Application has stopped.")