import time
import spidev
import wiringpi as wp
from enum import Enum
from threading import Thread, Event
from smbus2 import SMBus


############
# Base Pin #
############
class __BasePin():
    # Private basis klasse voor alle GPIO pinnen
    def __init__(self, pin: int) -> None:
        self.pin: int = pin
        wp.wiringPiSetup()


########################
# LightSensor : BH1750 #
########################
class BH1750():
    # Service klasse voor de BH1750
    def __init__(self, bus: SMBus = SMBus(0), address: int = 0x23) -> None:
        self.bus: SMBus = bus
        self.address: int = address
        self.lux_read = 0.0
        self.__lux_ready = Event()
        self.__lux_thread = Thread(target=self.__get_lux, daemon=True)
        self.__lux_thread.start()

    # Private methode om de hoeveelheid Lux terug te krijgen en op te slaan in een variabele, threading (non-blocking)
    def __get_lux(self) -> None:
        while True:
            self.bus.write_byte(self.address, 0x10)
            time.sleep(0.2)
            data = self.bus.read_i2c_block_data(self.address, 0x00, 2)
            self.lux_read = (data[0] << 8 | data[1]) / 1.2
            self.__lux_ready.set()
    
    # Geef opgeslagen (variabele) resultaat van de lux lezing terug
    def get_lux(self) -> float:
        self.__lux_ready.clear()
        return self.lux_read


##########################
# AD Convertor : MCP3008 #
##########################
class MCP3008():
    # Service klasse voor de MCP3008, gebruikt SPI
    def __init__(self, bus: int = 0, device: int = 0) -> None:
        self.spi = spidev.SpiDev()
        self.spi.open(bus, device)

    # Lees wwarde van SPI kanaal en geef deze etrug
    def read_channel(self, channel: int):
        adc = self.spi.xfer2([1, (8 + channel) << 4, 0])
        data = ((adc[1] & 3) << 8) + adc[2]
        return data
    

###############
# Push Button #
###############
class PushButton(__BasePin):
    # Enum voor de status van de knop
    class State(str, Enum):
        PUSHED = "Pushed",
        RELEASED = "Released"

    # Service klasse voor de drukknop
    def __init__(self, pin: int) -> None:
        super().__init__(pin)
        self.button_state: self.State = self.State.RELEASED
        self.__btn_ready = Event()
        self.__btn_thread = Thread(target=self.__get_button_state, daemon=True)
        self.__btn_thread.start()
        wp.pinMode(self.pin, 0)
        wp.pullUpDnControl(self.pin, wp.PUD_DOWN)

    # Private methode om de knop status terug te krijgen en op te slaan in een variabele, threading (non-blocking)
    def __get_button_state(self):
        while True:
            self.button_state = self.State.PUSHED \
                if wp.digitalRead(self.pin) == wp.LOW \
                else self.State.RELEASED
            self.__btn_ready.set()
            time.sleep(0.1)

    # Geef opgeslagen (variabele) resultaat van de knop status terug
    def get_button_state(self):
        self.__btn_ready.clear()
        return self.button_state
    

#######
# LED #
#######
class LED(__BasePin):
    # Service klasse voor de LED
    def __init__(self, pin: int) -> None:
        super().__init__(pin)
        wp.softPwmCreate(self.pin, 0, 100)

    # Zet LED aan via PWM
    def turn_on(self):
        wp.softPwmWrite(self.pin, 100)

    # Schakel LED uit via PWM
    def turn_off(self):
        wp.softPwmWrite(self.pin, 0)

    # Regel de felheid van de LED via PWM
    def pwm_adjust(self, value):
        wp.softPwmWrite(self.pin, value)


###########
# RGB LED #
###########
class RgbLED():
    # Service klasse voor de RGB LED
    def __init__(self, r_pin: int, g_pin: int, b_pin: int) -> None:
        self.red: LED = LED(r_pin)
        self.green: LED = LED(g_pin)
        self.blue: LED = LED(b_pin)
    
    # Zet rode kleur aan
    def red_on(self):
        self.red.turn_on()
        self.green.turn_off()
        self.blue.turn_off()

    # Zet groene kleur aan
    def green_on(self):
        self.red.turn_off()
        self.green.turn_on()
        self.blue.turn_off()

    # Zet blauwe kleur aan
    def blue_on(self):
        self.red.turn_off()
        self.green.turn_off()
        self.blue.turn_on()

    # Zet LED compleet af
    def turn_off(self):
        self.red.turn_off()
        self.green.turn_off()
        self.blue.turn_off()
