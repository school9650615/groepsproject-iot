import time
import threading
import paho.mqtt.client as mqtt


###############
# MQTT Client #
###############
class MqttClient:
    # Service klasse om een MQTT client op te zetten
    def __init__(self, client_id: str, channel_id: str, username: str, password: str, host: str = "mqtt3.thingspeak.com") -> None:
        self.client: mqtt.Client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, client_id)
        self.mqtt_host = host
        self.topic = f"channels/{channel_id}/publish"
        self.data = None
        self.data_lock = threading.Lock()
        self.client.username_pw_set(username, password=password)
        self.connect()

    # Publiceer data naar MQTT broker
    def publish_data(self, data):
        try:
            self.client.publish(self.topic, payload=data)
        except OSError:
            self.client.reconnect()

    # Publiceer elke interval aan sec data op de MQTT broker (blocking)
    def continuous_publish(self, interval=15):
        while True:
            if self.data != None:
                self.publish_data(self.data)
                time.sleep(interval)

    # Start thread op de publish data functie (non-blocking)
    def start_continuous_publishing(self, interval=15):
        publish_thread = threading.Thread(target=self.continuous_publish, args=(interval,))
        publish_thread.daemon = True
        publish_thread.start()

    # Verbind met MQTT broker
    def connect(self):
        self.client.connect(self.mqtt_host)
        self.client.loop_start()

    # Verbreek verbinding met MQTT broker
    def disconnect(self):
        self.client.disconnect()

    # Update de data die verstuurd moet worden
    def update_data(self, data):
        with self.data_lock:
            self.data = data
