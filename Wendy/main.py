import time 
from bmp280 import BMP280
from smbus2 import SMBus, i2c_msg
import paho.mqtt.client as mqtt

#Create an I2C bussen object for temp and pressure  AND for 
bus = SMBus(0)
address_bmp = 0x76
address_bh = 0x23

#Setup BMP280
bmp280 = BMP280(i2c_addr=address_bmp,i2c_dev=bus)
interval = 15

# Setup BH1750
bus.write_byte(address_bh, 0x10)
bytes_read = bytearray(2)

# Measure light
def get_value(bus, address):
    write = i2c_msg.write(address, [0x10]) # 1lx resolution 120ms see datasheet
    read = i2c_msg.read(address, 2)
    bus.i2c_rdwr(write, read)
    bytes_read = list(read)
    return (((bytes_read[0]&3)<<8) + bytes_read[1])/1.2 # conversion see datasheet

# MQTT settings
MQTT_HOST = "mqtt3.thingspeak.com"                      
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL =60
MQTT_BMP_TOPIC_SUBSCRIBE = f"channels/2492588/subscribe/fields"
MQTT_TOPIC = "channels/2492588/publish"
MQTT_TOPIC_BH1750 = "channels/2502066/publish"
MQTT_CLIENT_ID = "AwwTHDgRGgklBjk3DysTJCI" 
MQTT_USER = "AwwTHDgRGgklBjk3DysTJCI"  
MQTT_PWD = "XgX2YFylwd1p0Smnf7xSEErO" 


def on_connect(client, userdata, flags, rc):
    if rc==0:
        print("Connected OK with result code "+str(rc))
        
    else:
        print("Bad connection with result code "+str(rc))

def on_disconnect(client, userdata, flags, rc=0):
    print("Disconnected result code "+str(rc))

def on_message(client,userdata,msg):
    print("Received a message on topic: " + msg.topic + "; message: " + msg.payload)
    
def on_chart1_message(client, userdata, msg):
    print()
    print("Temperatuurwaarde van Dennis: " + msg.topic + "; message: " + msg.payload.decode())

def on_chart2_message(client, userdata, msg):
    print("Luchtdrukwaarde van Dennis: " + msg.topic + "; message: " + msg.payload.decode())
    
# Set up a MQTT Client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, MQTT_CLIENT_ID) 
client.username_pw_set(MQTT_USER, MQTT_PWD)

# Set the callback for messages on fields 1 and 2 topic
client.message_callback_add(MQTT_BMP_TOPIC_SUBSCRIBE + "/field1", on_chart1_message)
client.message_callback_add(MQTT_BMP_TOPIC_SUBSCRIBE + "/field2", on_chart2_message)

# Connect callback handlers to client
client.on_connect= on_connect
client.on_disconnect= on_disconnect
client.on_message= on_message

print("Attempting to connect to %s" % MQTT_HOST)
client.connect(MQTT_HOST, MQTT_PORT)
client.loop_start() #start the loop

while True: 
    #Measure and print data
    bmp280_temperature = bmp280.get_temperature()
    bmp280_pressure = bmp280.get_pressure()
    lux = get_value(bus, address_bh)
    print()
    print("Lichthelderheid in Lux: {:.2f}".format(lux))
    
    print("Temperature in Celsius: %4.1f" % (bmp280_temperature)) 
    print("Luchtdruk in hPa: %4.1f"% (bmp280_pressure))
  
    #Create the JSON data structure
    MQTT_DATA = "field7="+str(bmp280_temperature)+"&field8="+str(bmp280_pressure)+"&status=MQTTPUBLISH"
    MQTT_DATA_BH1750 = "field1="+str(lux)+"&status=MQTTPUBLISH" 
      
    try:
        client.publish(topic=MQTT_TOPIC, payload=MQTT_DATA, qos=0, retain=False, properties=None) #data temperatuur en luchtdruk
        client.publish(topic=MQTT_TOPIC_BH1750, payload=MQTT_DATA_BH1750, qos=0, retain=False, properties=None) #data lichthelderheid
        client.subscribe(topic=MQTT_BMP_TOPIC_SUBSCRIBE + "/field1") # Dennis temperatuurveld
        client.subscribe(topic=MQTT_BMP_TOPIC_SUBSCRIBE + "/field2") # Dennis luchtdruk
        
        time.sleep(interval)
    except OSError:
        client.reconnect()