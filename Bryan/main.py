import time
import sys
from smbus2 import SMBus
from bmp280 import BMP280
from service.parts import BH1750, MCP3008, PushButton, LED, RgbLED
from service.mqtt import MqttClient


################
# MAIN FUNCTIE #
################
def main():

    # Initialisaties van componenten (klasses)
    bh1750 = BH1750()
    bmp208 = BMP280(i2c_dev=SMBus(0))
    mcp3008 = MCP3008(bus=1, device=0)
    pushbutton = PushButton(pin=10)
    rgb_led = RgbLED(r_pin=3, g_pin=4, b_pin=6)
    pwm_led = LED(pin=2)

    # Declaraties van intiele waarden
    pwm_state = 0
    choice_index = 0
    desired_lux = 0
    desired_temp = 0
    desired_press = 0

    # Initialisaties van MQTT client (GEVOELIGE INFO IS WEGELATEN)
    lux_mqtt_client = MqttClient("CLIENT_ID", "CHANNEL", "USERNAME", "PASSWORD")
    tp_mqtt_client = MqttClient("CLIENT_ID", "CHANNEL", "USERNAME", "PASSWORD")
    
    # Start de doorlopende publish naar de MQTT broker (thingspeak)
    lux_mqtt_client.start_continuous_publishing(15)
    tp_mqtt_client.start_continuous_publishing(15)
    
    ########
    # LOOP #
    ########
    while True:
        try:
            # Verkrijg de waarden van de componenten
            lux = int(bh1750.get_lux())
            temp = bmp208.get_temperature()
            press = bmp208.get_pressure()
            potentio = mcp3008.read_channel(0)
            button_state = pushbutton.get_button_state()

            # Controlleer of de knop ingeduwd is en verwissel naar volgende keuze index van de in te stellen potentiowaarde
            # De keuze tussen LUX, TEMP of DRUK
            if button_state == pushbutton.State.PUSHED:
                choice_index = (choice_index + 1) % 3

            # Afhankelijk van de keuze wordt de juiste "gewenste waarde" variabele voorzien van de potentiowaarde, de RGB LED veranderd van kleur
            # De waarden die worden opgeslagen worden eerst omgevormd tot een formaat dat bij de meting past
            if choice_index == 0:
                desired_lux = round((potentio / 1023.0) * 100)
                rgb_led.blue_on()
            elif choice_index == 1:
                desired_press = round((potentio / 1023.0) * 1025)
                rgb_led.green_on()
            elif choice_index == 2:
                desired_temp = round((potentio / 1023.0) * 60)
                rgb_led.red_on()


            # De PWM status van de LED voor de lux meter aan te sturen wordt hier gecontroleerd
            # De lux wordt vergelijkt met de gewenste lux, als deze niet overeenkomt wordt de onafhankelijke pwm_state variabele oftewel hoger of lager gemaakt
            # De pwm_state bepaald de te toe te kennen waarde aan de PWM LED
            # Hierdoor wordt de PWM LED niet onmiddelijk aangestuurd door de potentiometer, en heeft bv: extern licht ook invloed op de waarde
            # Als de PWM status onder 0 komt wordt deze aangepast naar 0
            if pwm_state < 0:
                pwm_state = 0
            if lux != desired_lux:
                if lux < desired_lux:
                    pwm_state = pwm_state + 1
                elif lux > desired_lux:
                    pwm_state = pwm_state - 1
                else:
                    pwm_state = desired_lux
                pwm_led.pwm_adjust(pwm_state)
                
                
            # Print alles uit in de terminal
            sys.stdout.write("\033c")
            print("Potentio: " + str(potentio))
            print("Button: " + button_state)
            print("Choice Index: " + ("BLUE = Lux" if choice_index == 0 else ("GREEN = Pressure" if choice_index == 1 else "RED = Temperature")))
            print("\n")
            print("Lux: " + str(lux))
            print("PWM State: " + str(pwm_state))
            print("Desired Lux: " + str(desired_lux) + "\n")
            print("Pressure: " + "%.2f" %press)
            print("Desired Pressure: " + str(desired_press) + " Pa" + "\n")
            print("Temperature: " + "%.2f" % temp)
            print("Desired Temperature: " + str(desired_temp) + " °C")
            print("\n\n")

            # Publiceer de waarden naar de juiste aangemaakte kanalen van thingspeak via MQTT
            lux_mqtt_client.update_data("field3=" + str(lux) + "&field5=" + str(desired_temp) + "&field6=" + str(desired_press) + "&field7=" + str(desired_lux) + "&status=MQTTPUBLISH")
            tp_mqtt_client.update_data("field3=" + str(temp) + "&field4=" + str(press) + "&status=MQTTPUBLISH")

            time.sleep(0.1)

        # Sluit het programma netjes af bij een exception
        except:
            lux_mqtt_client.disconnect()
            pwm_led.turn_off()
            rgb_led.turn_off()
            exit(0)


########
# MAIN #
########
if __name__ == "__main__":
    main()
