import wiringpi as wp
import time as t
from smbus2 import SMBus, i2c_msg
import paho.mqtt.client as mqtt
from bmp280 import BMP280

from dotenv import load_dotenv
import os
import threading

load_dotenv()

exit_event = threading.Event()

#region Setup

# Global variables to store sensor readings
lux = 0.0
bmp280_temperature = 0.0
bmp280_pressure = 0.0

# Create an I2C bus object
I2C_BUS = SMBus(0)
ADDRESS_BH1750 = 0x23 # The default I2C address for BH1750 is 0x23.
ADDRESS_BMP280 = 0x76  # The default I2C address for BMP280 is 0x76.

# GPIO pins
PIN_LED_RGB_RED = 3
PIN_LED_RGB_GREEN = 4

# Define the hysteresis gap and interval
hysteresis_gap = 5
interval = 15 # Sample period in seconds

# Thresholds
threshold_lux = 60
threshold_temp = 15
threshold_press = 995

# Setup Wiringpi
wp.wiringPiSetup()

# Output pins
for pin in [PIN_LED_RGB_RED, PIN_LED_RGB_GREEN]:
    wp.pinMode(pin, wp.OUTPUT)

# Setup BH1750
I2C_BUS.write_byte(ADDRESS_BH1750, 0x10)
bytes_read = bytearray(2)

# Setup BMP280
bmp280 = BMP280(i2c_addr= ADDRESS_BMP280, i2c_dev=I2C_BUS)

# Array to keep track of LEDs that should be on
active_leds = []

#endregion

#region MQTT

# MQTT settings
MQTT_HOST ="mqtt3.thingspeak.com"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
BMP_CHANNEL_ID = os.getenv('GROUPPROJECT_BMP_CHANNEL_ID')
MQTT_BMP_TOPIC = f"channels/{BMP_CHANNEL_ID}/publish"
BH_CHANNEL_ID = os.getenv('GROUPPROJECT_BH_CHANNEL_ID')
MQTT_BH_TOPIC = f"channels/{BH_CHANNEL_ID}/publish"
MQTT_CLIENT_ID = os.getenv('GROUPPROJECT_MQTT_CLIENT_ID')
MQTT_USER = os.getenv('GROUPPROJECT_MQTT_USER')
MQTT_PWD = os.getenv('GROUPPROJECT_MQTT_PWD')

def on_connect(client, userdata, flags, rc):
    if rc==0:
        print("Connected OK with result code "+str(rc))
    else:
        print("Bad connection with result code "+str(rc))

def on_disconnect(client, userdata, flags, rc=0):
    print("Disconnected result code "+str(rc))

def on_message(client, userdata, msg):
    global lux
    print(f"Received a message on topic: {msg.topic}; message: {msg.payload.decode('utf-8')}")

# Set up a MQTT Client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, MQTT_CLIENT_ID)
client.username_pw_set(MQTT_USER, MQTT_PWD)

# Connect callback handlers to client
client.on_connect= on_connect
client.on_disconnect= on_disconnect
client.on_message= on_message

print("Attempting to connect to %s" % MQTT_HOST)
client.connect(MQTT_HOST, MQTT_PORT)
client.loop_start() #start the loop

def get_value(bus, address):
    write = i2c_msg.write(address, [0x10]) # 1lx resolution 120ms see datasheet
    read = i2c_msg.read(address, 2)
    bus.i2c_rdwr(write, read)
    bytes_read = list(read)
    return (((bytes_read[0]&3)<<8) + bytes_read[1])/1.2 # conversion see datasheet

def handle_bmp280_data(pin, threshold, data):
    if data > threshold + hysteresis_gap:
        wp.digitalWrite(pin, wp.HIGH)
    else:
        wp.digitalWrite(pin, wp.LOW)

#endregion

def cleanup():
    # Turn off the LEDs
    for pin in [PIN_LED_RGB_RED, PIN_LED_RGB_GREEN]:
        wp.digitalWrite(pin, wp.LOW)

    I2C_BUS.close()
    client.disconnect()  # Disconnect from the broker

def read_bh1750_data():
    global lux
    while True:
        lux = get_value(I2C_BUS, ADDRESS_BH1750)
        print("{:.2f} Lux".format(lux))
        
        if lux > threshold_lux + hysteresis_gap:
            led_pin_lux = PIN_LED_RGB_GREEN
        else:
            led_pin_lux = PIN_LED_RGB_RED

        # Turn off all LEDs
        for pin in [PIN_LED_RGB_RED, PIN_LED_RGB_GREEN]:
            wp.digitalWrite(pin, wp.LOW)

        # Turn on the selected LED
        wp.digitalWrite(led_pin_lux, wp.HIGH)

        t.sleep(3)

        if exit_event.is_set():
            break
        

def read_bmp280_data():
    global bmp280_temperature, bmp280_pressure
    while True:
        bmp280_temperature = bmp280.get_temperature()
        bmp280_pressure = bmp280.get_pressure()
        print("Temperature: %4.1f, Pressure: %4.1f" % (bmp280_temperature, bmp280_pressure))
        
        if bmp280_temperature > threshold_temp + hysteresis_gap or bmp280_pressure > threshold_press + hysteresis_gap:
            wp.digitalWrite(PIN_LED_RGB_RED, wp.HIGH)
        else:
            wp.digitalWrite(PIN_LED_RGB_RED, wp.LOW)

        t.sleep(3)

        if exit_event.is_set():
            break

def publish_mqtt_data():
    while True:
        global bmp280_temperature, bmp280_pressure, lux
        MQTT_BMP_DATA = "field1=" + str(bmp280_temperature) + "&field2=" + str(bmp280_pressure) + "&status=MQTTPUBLISH"
        MQTT_BH_DATA = "field4=" + str(lux) + "&status=MQTTPUBLISH"

        try:
            client.publish(topic=MQTT_BMP_TOPIC, payload=MQTT_BMP_DATA, qos=0, retain=False, properties=None)
            client.publish(topic=MQTT_BH_TOPIC, payload=MQTT_BH_DATA, qos=0, retain=False, properties=None)
            print("Published data!")
        except OSError:
            client.reconnect()

        t.sleep(interval)
        if exit_event.is_set():
            break

def subscribe_fields():
    try:
        client.subscribe(topic=f'channels/{BH_CHANNEL_ID}/subscribe/fields/field1')
        client.subscribe(topic=f'channels/{BH_CHANNEL_ID}/subscribe/fields/field2')
        client.subscribe(topic=f'channels/{BH_CHANNEL_ID}/subscribe/fields/field3')
    except OSError:
        print("Error subscribing to topics.")
        client.reconnect()

# Create threads
thread_bh1750 = threading.Thread(target=read_bh1750_data)
thread_bmp280 = threading.Thread(target=read_bmp280_data)
thread_mqtt = threading.Thread(target=publish_mqtt_data)

# Start threads
thread_bh1750.start()
thread_bmp280.start()
thread_mqtt.start()

try:
    subscribe_fields()

    # Keep the main thread running
    while True:
        t.sleep(1)
except KeyboardInterrupt:
    cleanup()
    exit_event.set()
    print("\nProgram terminated")